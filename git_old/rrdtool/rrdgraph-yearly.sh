#!/bin/sh
/usr/bin/rrdtool graph /mirror/root/stats_yearly.png \
-s -1y \
--imgformat=PNG \
--title='Mirror Traffic' \
--base=1000 \
--height=120 \
--width=600 \
--alt-autoscale-max \
--lower-limit=0 \
--vertical-label='bits per second' \
--slope-mode \
--font TITLE:10: \
--font AXIS:8: \
--font LEGEND:8: \
--font UNIT:8: \
DEF:a="/var/rrdtool/cogent.rrd":snmp_oid:AVERAGE \
DEF:b="/var/rrdtool/orion.rrd":snmp_oid:AVERAGE \
DEF:c="/var/rrdtool/campus.rrd":snmp_oid:AVERAGE \
CDEF:cdefa=a,8,* \
CDEF:cdefe=b,8,* \
CDEF:cdefi=c,8,* \
CDEF:cdefbc=TIME,1318318854,GT,a,a,UN,0,a,IF,IF,TIME,1318318854,GT,b,b,UN,0,b,IF,IF,TIME,1318318854,GT,c,c,UN,0,c,IF,IF,+,+,8,* \
AREA:cdefa#157419FF:"Cogent"  \
GPRINT:cdefa:LAST:"Current\:%8.2lf%s"  \
GPRINT:cdefa:AVERAGE:"Average\:%8.2lf%s"  \
GPRINT:cdefa:MAX:"Maximum\:%8.2lf%s\n"  \
AREA:cdefe#00CF00FF:"Orion":STACK \
GPRINT:cdefe:LAST:" Current\:%8.2lf%s"  \
GPRINT:cdefe:AVERAGE:"Average\:%8.2lf%s"  \
GPRINT:cdefe:MAX:"Maximum\:%8.2lf%s\n"  \
AREA:cdefi#EE5019FF:"Campus":STACK \
GPRINT:cdefi:LAST:"Current\:%8.2lf%s"  \
GPRINT:cdefi:AVERAGE:"Average\:%8.2lf%s"  \
GPRINT:cdefi:MAX:"Maximum\:%8.2lf%s\n"  \
LINE1:cdefbc#000000FF:"Total"  \
GPRINT:cdefbc:LAST:" Current\:%8.2lf%s"  \
GPRINT:cdefbc:AVERAGE:"Average\:%8.2lf%s"  \
GPRINT:cdefbc:MAX:"Maximum\:%8.2lf%s\n" >/dev/null 2>/dev/null
