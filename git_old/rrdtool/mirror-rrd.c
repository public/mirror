#include "mirror-nl-glue.h"
#include <rrd.h>

int main(void) {
	char *argv[3];
	unsigned long packet_count;

	switch(fork()) {
		case -1:
			return -1;
		case 0:
			close(0);
			close(1);
			setsid();
			break;
		default:
			_exit(0);
	}	

    mirror_stats_initialize();
    argv[0] = malloc(1024);	
    for (;;) {
        packet_count = get_class_byte_count(&cogent_class);
	snprintf(argv[0], 1024, "N:%lu", packet_count);
	rrd_update_r("/var/rrdtool/cogent.rrd", NULL, 1, argv);
        packet_count = get_class_byte_count(&orion_class);
	snprintf(argv[0], 1024, "N:%lu", packet_count);
	rrd_update_r("/var/rrdtool/orion.rrd", NULL, 1, argv);
        packet_count = get_class_byte_count(&campus_class);
	snprintf(argv[0], 1024, "N:%lu", packet_count);
	rrd_update_r("/var/rrdtool/campus.rrd", NULL, 1, argv);
   	if (rrd_test_error()) {
        	fprintf(stderr, "ERROR: %s\n", rrd_get_error());
        	rrd_clear_error();
	}
        sleep(5);
        mirror_stats_refresh();
    }
}

