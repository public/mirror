+++
title = "Scheduled outage on Sunday, Dec 22 2024"
date = "2024-12-19"
+++

**Update (3:30PM)**: Mirror is up again. Thank you for your patience!

CSC Mirror is expected to be offline on Sunday, Dec 22 from 7:00am to 4:00pm due to a planned power outage in MC server room ([UW PlantOps](https://plantops.uwaterloo.ca/service-interruptions/notice.php?ID=2433)). There may also be intermittent disruptions on Friday, December 20th in preparation for the power outage.

Sorry for the service disruption, and if you have any questions or concerns, please let us know on [syscom@csclub.uwaterloo.ca](syscom@csclub.uwaterloo.ca).
