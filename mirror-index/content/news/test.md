+++
title = "New mirror index page"
date = "2023-05-04"
+++

We've updated the mirror index page to include more detailed synchronization status information.

If you experienced any usability issues due to browser compatibility, please let us know on [syscom@csclub.uwaterloo.ca](syscom@csclub.uwaterloo.ca).
