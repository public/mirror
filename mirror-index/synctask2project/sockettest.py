#!/usr/bin/env python3

import socket, sys, time
def recvall(sock):
    BUFF_SIZE = 4096 # 4 KiB
    data = b''
    while True:
        part = sock.recv(BUFF_SIZE)
        data += part
        if len(part) < BUFF_SIZE:
            # either 0 or end of data
            break
    return data

sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
merlin_socket = "/home/mirror/merlin/merlin.sock"
sock.connect(merlin_socket)

command = "status"
sock.sendall(str.encode(command))
sock.shutdown(socket.SHUT_WR)
result = recvall(sock).decode("utf-8")
print(result)
