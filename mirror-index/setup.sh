#!/bin/bash
# Setup mirror-index

set -euo pipefail

# Create directories
mkdir -p data

# Run deploy once
./deploy.sh

# Copy assets to destination
cp public/include/* /mirror/root/include/
