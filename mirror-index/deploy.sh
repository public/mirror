#!/bin/bash
# Deploy mirror index page
# Note: This script is only used for deployment (commonly used in cron)
#       To build dependencies and do initial setup, run setup.sh

set -euo pipefail

# Generate json
cd ./synctask2project/
./synctask2project /mirror/root/sync.json

# Run page generation
cd ../
cp /mirror/root/sync.json ./data/
hugo > /dev/null

# Copy to mirror root
cp public/index.html /mirror/root/index.html
cp public/project_table/index.html  /mirror/root/include/project_table.html
cp -r public/news /mirror/root/
