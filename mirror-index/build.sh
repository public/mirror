#!/bin/bash
# Build static dependencies
# Requires: postcss-cli, autoprefixer, minify (all from npm)
# I recommend yarn global but do whatever you want

# Build auto-prefix-ed, minified CSS
sass assets/csc-mirror.sass | postcss --use autoprefixer | minify --css > ./static/include/mirror-index-2024.css

# Compress CSS & JS assets
# To be used alongside nginx's gzip-static module
gzip -c ./static/include/htmx.min.js           > ./static/include/htmx.min.js.gz
gzip -c ./static/include/mirror-index-2024.css > ./static/include/mirror-index-2024.css.gz
gzip -c ./static/include/csc-logo.svg          > ./static/include/csc-logo.svg.gz
