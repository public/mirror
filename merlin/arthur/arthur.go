package arthur

import (
	"bufio"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net"
	"os"
	"path/filepath"
	"sort"
	"strings"

	"git.csclub.uwaterloo.ca/public/merlin/config"
	"git.csclub.uwaterloo.ca/public/merlin/logger"
	"git.csclub.uwaterloo.ca/public/merlin/sync"
)

type RepoStatusInfo struct {
	Name                           string `json:"name"`
	IsRunning                      bool   `json:"is_running"`
	LastAttemptStartTime           int64  `json:"last_attempt_time"`
	LastAttemptExit                string `json:"last_attempt_exit"`
	LastSuccessfulAttemptStartTime int64  `json:"last_successful_attempt_time,omitempty"`
	NextSyncTime                   int64  `json:"next_sync_time"`
}
type StatusInfo struct {
	Repos []*RepoStatusInfo `json:"repos"`
}

// Reads and parses the message sent over the accepted connection
func GetCommand(conn net.Conn) (command, repoName string) {
	command = ""
	repoName = ""

	buf, err := ioutil.ReadAll(conn)
	if err != nil {
		logger.ErrLog(err.Error())
		return
	}

	args := strings.Split(string(buf), ":")
	if len(args) >= 1 {
		command = args[0]
	}
	if len(args) >= 2 {
		repoName = args[1]
	}
	return
}

// Receives an message and writes it to both stdout and the accepted connection
func SendAndLog(conn net.Conn, msg string) {
	logger.OutLog(msg)
	conn.Write([]byte(msg))
}

// Writes the status of the repos to the accepted connection
func SendStatus(conn net.Conn) {
	// get the names of all of the repos in the config
	var keys []string
	for name := range config.RepoMap {
		keys = append(keys, name)
	}
	sort.Strings(keys)

	repoStatuses := make([]*RepoStatusInfo, len(keys))
	for i, name := range keys {
		repo := config.RepoMap[name]
		repoStatuses[i] = &RepoStatusInfo{
			Name:                           name,
			IsRunning:                      repo.State.IsRunning,
			LastAttemptStartTime:           repo.State.LastAttemptStartTime,
			LastAttemptExit:                config.StatusToString(repo.State.LastAttemptExit),
			LastSuccessfulAttemptStartTime: repo.State.LastSuccessfulAttemptStartTime,
		}
		if repo.State.LastAttemptStartTime != 0 {
			repoStatuses[i].NextSyncTime = repo.State.LastAttemptStartTime + int64(repo.Frequency)
		}
	}
	statusInfo := StatusInfo{Repos: repoStatuses}
	marshaledStatusInfo, err := json.Marshal(&statusInfo)
	if err != nil {
		logger.ErrLog(err)
		return
	}
	writer := bufio.NewWriter(conn)
	writer.Write(marshaledStatusInfo)
	err = writer.Flush()
	if err != nil {
		logger.ErrLog(err)
	}
}

// Attempt to force the sync of the repo. Returns true iff a sync was started.
func ForceSync(conn net.Conn, repoName string) (newSync bool) {
	newSync = false

	if repo, isInMap := config.RepoMap[repoName]; isInMap {
		logger.OutLog("Attempting to force sync of " + repoName)
		if sync.SyncIfPossible(repo, true) {
			conn.Write([]byte("Forced sync for " + repoName))
			newSync = true
		} else {
			SendAndLog(conn, "Could not force sync: "+repoName+" is already syncing.")
		}
	} else {
		SendAndLog(conn, repoName+" is not tracked so cannot sync")
	}
	return
}

// Create and start listening to a unix socket and accept and forward connections to the connChan channel
func StartListener(connChan chan net.Conn, stopLisChan <-chan struct{}, stopLisAckChan chan<- struct{}) {
	sockpath := config.Conf.SockPath
	if filepath.Ext(sockpath) != ".sock" {
		panic(fmt.Errorf("socket file must end with .sock"))
	}

	// Attempt to remove sockpath if exists, continue if does not exist.
	// If old sockpath is not removed then will get "bind: address already in use".
	if _, err := os.Stat(sockpath); err == nil { // sockpath exists
		if err := os.Remove(sockpath); err != nil {
			panic(err)
		}
	} else if !errors.Is(err, os.ErrNotExist) { // error is not that the sockpath does not exist
		panic(err)
	}

	// creates and starts listening to a unix socket
	ear, err := net.Listen("unix", sockpath)
	if err != nil {
		panic(err)
	}
	logger.OutLog("Listening to unix socket at " + sockpath)

	go func() {
		for {
			// Note: cannot use select with ear.Accept() for some reason (it does block until connection is made)
			conn, err := ear.Accept()
			if err != nil {
				if errors.Is(err, net.ErrClosed) {
					// Expected behaviour
				} else {
					logger.ErrLog("Unhandlable socket error: " + err.Error())
					ear.Close()
				}
				stopLisAckChan <- struct{}{}
				return
			}
			connChan <- conn
		}
	}()

	// wait for stopLisChan to close or signal to close the socket listener
	<-stopLisChan
	ear.Close()
}
