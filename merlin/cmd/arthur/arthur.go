package main

import (
	"bytes"
	_ "embed"
	"encoding/json"
	"flag"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net"
	"os"
	"os/exec"
	"regexp"
	"strconv"
	"strings"
	"text/tabwriter"
	"time"

	serverArthurPkg "git.csclub.uwaterloo.ca/public/merlin/arthur"
)

var DEFAULT_SOCKET_PATH = "/mirror/merlin/run/merlin.sock"

var HELP_MESSAGE = `USAGE:
arthur [-h|--help] [-json] [-sock <socket_path>] COMMAND

COMMANDS:
  sync:[repo]
  status

FLAGS:
`

//go:embed layout.html
var layoutHtmlTemplate string

type EnrichedRepoStatusInfo struct {
	serverArthurPkg.RepoStatusInfo
	Size uint64 `json:"size,omitempty"`
}
type EnrichedStatusInfo struct {
	Repos []*EnrichedRepoStatusInfo `json:"repos"`
}

func main() {
	log.SetPrefix("[ERROR]: ")
	log.SetFlags(0)

	flag.Usage = func() {
		w := flag.CommandLine.Output()

		fmt.Fprintf(w, HELP_MESSAGE)

		flag.PrintDefaults()
	}

	sockPath := flag.String("sock", DEFAULT_SOCKET_PATH, "alternate socket file")

	var shouldOutputJson bool
	flag.BoolVar(&shouldOutputJson, "json", false, "JSON output")
	var shouldOutputHtml bool
	flag.BoolVar(&shouldOutputHtml, "html", false, "HTML output")

	flag.Parse()

	if flag.NArg() < 1 {
		os.Exit(1)
	}
	command := flag.Args()[0]

	conn, err := net.Dial("unix", *sockPath)
	if err != nil {
		log.Fatal(err)
	}

	_, err = conn.Write([]byte(command))
	if err != nil {
		log.Fatal(err)
	}

	conn.(*net.UnixConn).CloseWrite()

	response, err := ioutil.ReadAll(conn)
	if err != nil {
		log.Fatal(err)
	}
	// TODO: merlin response should always be JSON
	if strings.HasPrefix(command, "sync") {
		fmt.Println(string(response))
		return
	}
	statusInfo := EnrichedStatusInfo{}
	err = json.Unmarshal(response, &statusInfo)
	if err != nil {
		log.Fatal(err)
	}

	if shouldOutputJson {
		filesystemSizes := getFilesystemSizes()
		for _, repoInfo := range statusInfo.Repos {
			repoInfo.Size = filesystemSizes[repoInfo.Name]
		}
		jsonOutput, jsonErr := json.MarshalIndent(&statusInfo, "", "  ")
		if jsonErr != nil {
			log.Fatal(jsonErr)
		}
		fmt.Println(string(jsonOutput))
	} else if shouldOutputHtml {
		funcs := template.FuncMap{
			"unixTimeToStr": unixTimeToStr,
			"minus":         func(a, b int64) int64 { return a - b },
		}
		tmpl := template.Must(template.New("layout").
			Funcs(funcs).
			Parse(layoutHtmlTemplate))
		var buf bytes.Buffer
		data := map[string]interface{}{
			"Repos": statusInfo.Repos,
			"Now":   time.Now().Unix(),
		}
		err = tmpl.Execute(&buf, data)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Print(buf.String())
	} else {
		writer := tabwriter.NewWriter(os.Stdout, 5, 5, 5, ' ', 0)
		// print out the state of each repo in the config (last and next sync time + if it is currently running)
		fmt.Fprintf(writer, "Repository\tLast Synced\tNext Expected Sync\tLast Exit\tRunning\n")
		for _, repo := range statusInfo.Repos {
			fmt.Fprintf(writer, "%s\t%s\t%s\t%s\t%t\n",
				repo.Name,
				unixTimeToStr(repo.LastAttemptStartTime),
				unixTimeToStr(repo.NextSyncTime),
				repo.LastAttemptExit,
				repo.IsRunning,
			)
		}
		writer.Flush()
	}
}

var location *time.Location

func unixTimeToStr(unixTime int64) string {
	if unixTime == 0 {
		return "unknown"
	}
	if location == nil {
		var err error
		// Force arthur to send back time information in America/Toronto time
		location, err = time.LoadLocation("America/Toronto")
		if err != nil {
			location = time.UTC
		}
	}
	// for other ways to format the time see: https://pkg.go.dev/time#pkg-constants
	return time.Unix(unixTime, 0).In(location).Format(time.RFC1123)
}

// getFilesystemSizes parses the output of the `zfs list` command and returns
// a map which looks like {"debian":"1.62T","gnu":"148G",...}.
func getFilesystemSizes() map[string]uint64 {
	pattern := regexp.MustCompile(`^cscmirror\d/([^ ]+) (\d+)$`)
	result := make(map[string]uint64)
	// Use the absolute path to the zfs command because /sbin isn't in the
	// mirror user's PATH variable when running from cron
	cmd := exec.Command("sh", "-c", `/sbin/zfs list -t filesystem -H -p | awk '/^cscmirror[[:digit:]]\// {print $1 " " $2}'`)
	output, err := cmd.Output()
	if err != nil {
		fmt.Fprintf(os.Stderr, "zfs list failed: %+v\n", err)
		return result
	}
	lines := bytes.Split(output, []byte("\n"))
	for _, line := range lines {
		if len(line) == 0 {
			continue
		}
		matches := pattern.FindSubmatch(line)
		if matches == nil {
			continue
		}
		projectName := string(matches[1])
		diskUsage, parseErr := strconv.ParseUint(string(matches[2]), 10, 64)
		if parseErr != nil {
			fmt.Fprintf(os.Stderr, "could not parse int: %s\n", string(matches[2]))
			continue
		}
		result[projectName] = diskUsage
	}
	return result
}
