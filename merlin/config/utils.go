package config

import (
	"errors"
	"fmt"
	"os"
)

// panic if error is not null
func panicIfErr(e error) {
	if e != nil {
		panic(e)
	}
}

// create empty files with rw-r--r-- (644) if does not exist
func touch(files ...string) {
	for _, file := range files {
		if fileStat, err := os.Stat(file); errors.Is(err, os.ErrNotExist) {
			f, err := os.OpenFile(file, os.O_CREATE, 0644)
			if err != nil {
				panic(fmt.Errorf("unable to create file %s", file))
			}
			f.Close()
		} else if fileStat.IsDir() {
			panic(fmt.Errorf("%s is a directory", file))
		}
		err := os.Chmod(file, 0644)
		panicIfErr(err)
	}
}
