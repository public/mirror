package config

import (
	"errors"
	"os"
	"reflect"
	"testing"

	"github.com/davecgh/go-spew/spew"
)

func TestTouchFiles(t *testing.T) {
	files := []string{
		"/tmp/merlin_touch_test_1",
		"/tmp/merlin_touch_test_2",
		"/tmp/merlin_touch_test_3",
	}
	touch(files[0], files[1], files[2])
	for _, file := range files {
		if _, err := os.Stat(file); err != nil {
			t.Errorf(err.Error())
		} else if err := os.Remove(file); err != nil {
			t.Errorf(err.Error())
		}
	}
}

func TestPanicIfErr(t *testing.T) {
	panicIfErr(nil)
	defer func() { recover() }()
	panicIfErr(errors.New("AAAAAAAAAA"))
	t.Errorf("panicIfErr should have panicked")
}

func TestLoadConfig(t *testing.T) {
	doneChan := make(chan SyncResult)
	stopChan := make(chan struct{})
	LoadConfig("config_test.ini", doneChan, stopChan)
	expectedConfig := Config{
		Hostname:            "mirror.csclub.uwaterloo.ca",
		MaxJobs:             6,
		IPv4Address:         "129.97.134.129",
		IPv6Address:         "2620:101:f000:4901:c5c::129",
		DefaultSyncType:     "csc-sync-standard",
		DefaultFrequencyStr: "daily",
		DefaultMaxTime:      1000,
		DefaultMaxRsyncIO:   0,
		DownloadDir:         "test_files/download",
		StateDir:            "test_files",
		RepoLogDir:          "test_files/log",
		RsyncLogDir:         "test_files/rsync",
		ZfssyncLogDir:       "test_files/zfssync",
		SockPath:            "test_files/test.sock",
	}
	expectedRepo1 := Repo{
		Name:           "eelinux",
		SyncType:       "csc-sync-nonstandard",
		FrequencyStr:   "tri-hourly",
		Frequency:      10800,
		MaxTime:        2000,
		MaxRsyncIO:     100,
		LocalDir:       "eelinux",
		RsyncHost:      "rsync.releases.eelinux.ca",
		RsyncDir:       "releases",
		StateFile:      "test_files/eelinux",
		RepoLogFile:    "test_files/log/eelinux.log",
		Logger:         Repos[0].Logger,
		RsyncLogFile:   "test_files/rsync/eelinux-rsync.log",
		ZfssyncLogFile: "test_files/zfssync/eelinux-zfssync.log",
		State: &RepoState{
			IsRunning:            false,
			LastAttemptStartTime: 1600000000,
			LastAttemptExit:      1,
			DoneChan:             doneChan,
			StopChan:             stopChan,
		},
	}
	expectedRepo2 := Repo{
		Name:           "yoland",
		SyncType:       "csc-sync-standard",
		FrequencyStr:   "daily",
		Frequency:      86400,
		MaxTime:        1000,
		MaxRsyncIO:     0,
		LocalDir:       "yoland-releases",
		RsyncHost:      "rsync.releases.yoland.io",
		RsyncDir:       "releases",
		StateFile:      "test_files/yoland",
		RepoLogFile:    "test_files/log/yoland.log",
		Logger:         Repos[1].Logger,
		RsyncLogFile:   "test_files/rsync/yoland-rsync.log",
		ZfssyncLogFile: "test_files/zfssync/yoland-zfssync.log",
		State: &RepoState{
			IsRunning:            false,
			LastAttemptStartTime: 0,
			LastAttemptExit:      2,
			DoneChan:             doneChan,
			StopChan:             stopChan,
		},
	}

	if !reflect.DeepEqual(expectedConfig, Conf) {
		t.Errorf("Config loaded does not match expected config")
		spew.Dump(expectedConfig)
		spew.Dump(Conf)
	}
	if !reflect.DeepEqual(expectedRepo1, *Repos[0]) {
		t.Errorf("The eelinux repo loaded does not match the exected repo config")
		spew.Dump(expectedRepo1)
		spew.Dump(*Repos[0])
	}
	if !reflect.DeepEqual(expectedRepo2, *Repos[1]) {
		t.Errorf("The yoland repo loaded does not match the exected repo config")
		spew.Dump(expectedRepo2)
		spew.Dump(*Repos[1])
	}

	os.Remove("test_files/yoland")
	os.RemoveAll("test_files/log")
	os.RemoveAll("test_files/rsync")
	os.RemoveAll("test_files/zfssync")
}
