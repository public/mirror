module git.csclub.uwaterloo.ca/public/merlin

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/stretchr/testify v1.7.0 // indirect
	golang.org/x/sys v0.0.0-20220209214540-3681064d5158
	gopkg.in/ini.v1 v1.63.2
)

go 1.16
