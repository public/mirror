package main

import (
	"flag"
	"fmt"
	"net"
	"os"
	"os/signal"
	"syscall"
	"time"

	"golang.org/x/sys/unix"

	"git.csclub.uwaterloo.ca/public/merlin/arthur"
	"git.csclub.uwaterloo.ca/public/merlin/config"
	"git.csclub.uwaterloo.ca/public/merlin/logger"
	"git.csclub.uwaterloo.ca/public/merlin/sync"
)

const DEFAULT_CONFIG_PATH = "merlin-config.ini"

func main() {

	// custom help message
	flag.Usage = func() {
		w := flag.CommandLine.Output()

		fmt.Fprintf(w, "USAGE: merlin [-h | --help] [--config=<config-path>]\n")
		flag.PrintDefaults()
	}

	// parse command args
	// if more are added can prob use a library
	configPath := flag.String("config", DEFAULT_CONFIG_PATH, "alternate config file")
	flag.Parse()

	// check the user that program is running under (should be mirror)
	if os.Getuid() == 0 {
		panic("Merlin should not be run as root")
	}

	// receives a Result struct when a repo is done/fails syncing
	doneChan := make(chan config.SyncResult)
	// closed when merlin is told to stop running
	stopChan := make(chan struct{})

	// receives a Conn when a client makes a connection to unix socket
	connChan := make(chan net.Conn)
	// closed or receives a signal to stop listening to the unix socket
	stopLisChan := make(chan struct{})
	// Acknowledgement that the goroutine listening on the unix socket has cleaned up
	// Use a buffer size of 1 so that we don't have to drain it when shutting down
	stopLisAckChan := make(chan struct{}, 1)

	// gets unblocked when SIGINT or SIGTERM is sent, will begin process of stopping the program
	stopSig := make(chan os.Signal, 1)
	signal.Notify(stopSig, syscall.SIGINT, syscall.SIGTERM)

	// gets unblocked with SIGHUP is sent, will attempt to reload the config
	reloadSig := make(chan os.Signal, 1)
	signal.Notify(reloadSig, syscall.SIGHUP)

	unix.Umask(002)
	repoIdx := 0
	numJobsRunning := 0

	loadConfigAndStartListener := func() {
		config.LoadConfig(*configPath, doneChan, stopChan)
		logger.OutLog("Loaded config:\n" + fmt.Sprintf("%+v\n", config.Conf))

		// reset the round-robin index and recreate the socket listener
		repoIdx = 0
		go arthur.StartListener(connChan, stopLisChan, stopLisAckChan)
	}

	// We use a round-robin strategy. It's not the most efficient, but it's simple
	// (read: easy to understand) and guarantees each repo will eventually get a chance to run.
	runAsManyAsPossible := func() {
		startIdx := repoIdx

		for numJobsRunning < config.Conf.MaxJobs {
			if sync.SyncIfPossible(config.Repos[repoIdx], false) {
				numJobsRunning++
			}
			repoIdx = (repoIdx + 1) % len(config.Repos)
			if repoIdx == startIdx {
				// we've tried to run every repo and have come full circle
				return
			}
		}
	}

	loadConfigAndStartListener()
	// ensure that IsRunning is false otherwise repo will never sync.
	// only on startup can we assume that repos were not previously syncing
	// since reloading the config does not stop repos with a sync in progress
	for _, repo := range config.Repos {
		wasRunning := repo.State.IsRunning
		repo.State.IsRunning = false
		if wasRunning {
			// If some jobs were terminated early last time, try to resume them
			if sync.SyncIfPossible(repo, true) {
				numJobsRunning++
			}
		}
	}
	runAsManyAsPossible()

mainLoop:
	for {
		timer := time.NewTimer(1 * time.Minute)
		select {
		case <-stopSig: // caught a SIGINT or SIGTERM
			fmt.Println("Caught termination signal")
			// kill all syncing repos and the socket listener
			close(stopChan)
			close(stopLisChan)
			break mainLoop

		case <-reloadSig: // caught a SIGHUP
			// temporary stop the socket listener and load the config again
			stopLisChan <- struct{}{}
			// make sure that the current listener has really stopped before starting a new one
			<-stopLisAckChan
			loadConfigAndStartListener()

		case done := <-doneChan: // a sync is done and sends its exit status
			// repo could be removed from config while sync in progress
			if repo, check := config.RepoMap[done.Name]; check {
				sync.SyncCompleted(repo, done.Exit)
			}
			numJobsRunning--

		case conn := <-connChan: // a connection was accepted by the socket listener
			command, repoName := arthur.GetCommand(conn)
			switch command {
			case "status":
				arthur.SendStatus(conn)
			case "sync":
				if arthur.ForceSync(conn, repoName) {
					numJobsRunning++
				}
			default:
				arthur.SendAndLog(conn, "Received unrecognized command: "+command)
			}
			// close the received connection so that the message is sent
			conn.Close()

		case <-timer.C:
		}
		timer.Stop()
		runAsManyAsPossible()
	}

	// allow some time for jobs to terminate before force exiting the program
	go func() {
		time.Sleep(1 * time.Minute)
		logger.ErrLog("One minute has passed, forcefully exiting the program")
		os.Exit(1)
	}()

	// wait on repos to get terminated
	for {
		done := <-doneChan
		fmt.Println(done.Name + " finished after the termination signal, post-sync jobs will not run")
		// We do NOT want to call sync.SyncCompleted here for two reasons:
		// 1. the post-sync jobs might take a long time, and systemd will SIGKILL us if we take too long
		// 2. we want is_running to be saved as true in the state file so that we can resume this
		//    repo when we start back up
		numJobsRunning--
		if numJobsRunning <= 0 {
			os.Exit(0)
		}
	}
}
