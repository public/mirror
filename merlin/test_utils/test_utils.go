package test_utils

import (
	"os"
	"path"

	"git.csclub.uwaterloo.ca/public/merlin/config"
)

const TmpDir = "./test_tmp"

func removeTmpDir() {
	_, err := os.Stat(TmpDir)
	if err != nil {
		if os.IsNotExist(err) {
			return
		}
		panic(err)
	}
	err = os.RemoveAll(TmpDir)
	if err != nil {
		panic(err)
	}
}

func createTmpDir() {
	for _, subdir := range []string{"download/ubuntu"} {
		err := os.MkdirAll(path.Join(TmpDir, subdir), 0755)
		if err != nil {
			panic(err)
		}
	}
}

func SetupTestWithConfig(configPath string) (doneChan chan config.SyncResult, stopChan chan struct{}) {
	removeTmpDir()
	createTmpDir()
	doneChan = make(chan config.SyncResult)
	stopChan = make(chan struct{})
	config.LoadConfig(configPath, doneChan, stopChan)
	return
}

func SetupTest() (chan config.SyncResult, chan struct{}) {
	return SetupTestWithConfig("test_utils/merlin-config-test.ini")
}

func TeardownTest() {
	removeTmpDir()
}
