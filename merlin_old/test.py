#!/usr/bin/python

import merlin

merlin.repos = {
    'gnu': {
        'command': 'sleep 7',
        'interval': 30,
        'max-sync-time': 60,
    },
    'nongnu': {
        'command': 'sleep 10',
        'interval': 30,
        'max-sync-time': 60,
    },
    'mysql': {
        'command': 'sleep 30',
        'interval': 60,
        'max-sync-time': 60,
    },
    'mozdev': {
        'command': 'sleep 5',
        'interval': 5,
        'max-sync-time': 60,
    },
    'gnome': {
        'command': 'sleep 42',
        'interval': 10,
        'max-sync-time': 60,
    },
    'damnsmalllinux': {
        'command': 'sleep 3; exit 1',
        'interval': 15,
        'max-sync-time': 60,
    },
    'linuxmint': {
        'command': 'sleep 6',
        'interval': 20,
        'max-sync-time': 4,
    },
}

merlin.mintime = 10
merlin.earPath= 'merlin.sock'

if __name__ == '__main__':
    merlin.main()
