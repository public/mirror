#!/bin/bash
IFS=$'\n'
for i in `cat merlin.py | sed -ne "s/^[ ]\+'command':[ ]*'\([^']\+\).*$/\1/pg"`; do
	while [ "`jobs|wc -l`" -ge 9 ]; do
		sleep 1
	done
	echo "$i"
	logfile="`echo "$i" | cut -d' ' -f2 | sed -e's/\//_/g'`"
	bash -c "$i" >rebuild_logs/$logfile 2>&1 &
done
