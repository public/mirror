This script is to be used for Raspbian, which is NOT the same as Raspberry Pi OS.

It was downloaded from [here](https://raw.githubusercontent.com/plugwash/raspbian-tools/master/raspbmirror.py).

See [here](https://www.raspbian.org/RaspbianMirrors) for details about Raspbian mirroring.
